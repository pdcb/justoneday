// ##############################
// // // Tasks for TasksCard - see Dashboard view
// #############################

var bugs = [
  ["Sign contract for What are conference organizers afraid of?", "descripcion", "horario"],
  ["Sign contract for What are conference organizers afraid of?", "descripcion", "horario"],
  ["Sign contract for What are conference organizers afraid of?", "descripcion", "horario"],
  ["Sign contract for What are conference organizers afraid of?", "descripcion", "horario"]
];
var website = [
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];
var server = [
  "Lines From Great Russian Literature? Or E-mails From My Boss?",
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];
var aprobados = [
  "Dakota Jhonson, Jhon G Lake, Contabilidad",
  "Dakota Jhonson, Jhon G Lake, Administración",
  "Dakota Peasl, Jhon G Lake, Contabilidad",
  "Dakota Jhonson, Jhon G Lake, Contabilidad"
];var rechazados = [
  "Fred Jhonson, Jhon G Lake, Contabilidad",
  "Rice Jhonson, Jhon G Lake, Administración",
  "Dakota Jhonson, Jhon G Lake, Contabilidad",
  "Dakota Jhonson, Jhon G Lake, Contabilidad"
];var pendientes = [
  "Dakota Jhonson, Jhon G Lake, Contabilidad",
  "Dakota Jhonson, Jhon G Lake, Administración",
  "Dakota Jhonson, Jhon G Lake, Contabilidad",
  "Dakota Jhonson, Jhon G Lake, Contabilidad"
];
module.exports = {
  // these 3 are used to create the tasks lists in TasksCard - Dashboard view
  bugs,
  website,
  server,
  aprobados,
  rechazados,
  pendientes
};
