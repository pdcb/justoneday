import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons

import Check from "@material-ui/icons/Check";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Danger from "components/Typography/Danger.jsx";
import Success from "components/Typography/Success.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import TableCell from "@material-ui/core/TableCell";
import Close from "@material-ui/icons/Close";
import Edit from "@material-ui/icons/Edit";
import TableRow from "@material-ui/core/TableRow";

import tasksStyle from "assets/jss/material-dashboard-react/components/tasksStyle.jsx";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  tableUpgradeWrapper: {
    display: "block",
    width: "100%",
    overflowX: "auto",
    WebkitOverflowScrolling: "touch",
    MsOverflowStyle: "-ms-autohiding-scrollbar"
  },
  table: {
    width: "100%",
    maxWidth: "100%",
    marginBottom: "1rem",
    backgroundColor: "transparent",
    borderCollapse: "collapse",
    display: "table",
    borderSpacing: "2px",
    borderColor: "grey",
    "& thdead tr th": {
      fontSize: "1.063rem",
      padding: "12px 8px",
      verticalAlign: "middle",
      fontWeight: "300",
      borderTopWidth: "0",
      borderBottom: "1px solid rgba(0, 0, 0, 0.06)",
      textAlign: "inherit"
    },
    "& tbody tr td": {
      padding: "12px 8px",
      verticalAlign: "middle",
      borderTop: "1px solid rgba(0, 0, 0, 0.06)"
    },
    "& td, & th": {
      display: "table-cell"
    }
  },
  center: {
    textAlign: "center"
  }
};

function PuestosTrabajo(props) {
  const { classes } = props;
  return (
    <GridContainer >
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              Puestos de Trabajo
            </h4>
            <p className={classes.cardCategoryWhite}>
              En esta sección podrá visualizar y modificar los Puestos de Trabajo.
            </p>
            <Button style={{display:'flex', right: '0px', position:'absolute', margin: 20 }} color="primary" round="true">Agregar Puesto de Trabajo</Button>
          </CardHeader>
          <CardBody>
          <Table className={classes.table}
              tableHeaderColor="primary"
              tableHead={["Puesto de Trabajo", "Descripción", "Horario", "Requerimientos"]}
              tableData={[
                ["Programador", "El postulante podrá desarrollarse en el campo de Programación...", "8:00 - 13:00", "Mayor de 16 años"],
                ["Contador", "El postulante podrá desarrollarse en el campo de Contabilidad...", "8:00 - 13:00", "Solo mujeres"],
                ["Supervisor", "El postulante podrá desarrollarse en el campo de Supervisión...", "8:00 - 13:00", "Mayor de 17 años"],
                ["Asistente de Odontología", "El postulante podrá desarrollarse en el campo de Programación...", "8:00 - 13:00", "Tener cerificado de inglés intermedio."],
                ["Programador", "El postulante podrá desarrollarse en el campo de Programación...", "8:00 - 13:00", "Mayor de 16 años"]
              ]}
            >
            </Table>
           
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

export default withStyles(styles)(PuestosTrabajo);
